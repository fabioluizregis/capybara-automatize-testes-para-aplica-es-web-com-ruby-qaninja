describe 'Login com cadastro', :login3 do
    before(:each) do
        visit '/access'
    end

    it 'Login com sucesso' do

        login_form = find('#login')

        #fill_in preenche um campo, mapeado pelo name ou ID
        login_form.find('input[name=username]').set 'stark'
        login_form.find('input[name=password]').set 'jarvis!'

        sleep 1 #Sleep apenas para verificar visualmente o que está acontecendo
        #click_button clica em um botão pelo name
        click_button 'Entrar'

        expect(find('#flash')).to have_content 'Olá, Tony Stark. Você acessou a área logada!'
    end

    it 'Login com sucesso - forma 2' do
        within('#login') do
            find('input[name=username]').set 'stark'
            find('input[name=password]').set 'jarvis!'
            click_button 'Entrar'
        end
        expect(find('#flash')).to have_content 'Olá, Tony Stark. Você acessou a área logada!'
    end

    it 'Cadastro com sucesso' do
        within('#signup') do
            find('input[name=username]').set 'Fabio'
            find('input[name=password]').set 'minha_senha'
            find('a[id*=GetStartedButton]').click
            # * => contém
            # $ => tem no final
            # ^ => tem no início
        end
        expect(find('#result')).to have_content 'Dados enviados. Aguarde aprovação do seu cadastro!'
    end
end