describe 'alertas de javascript', :alerts do
    before(:each) do
        visit '/javascript_alerts'
    end

    it 'alerta' do
        click_button 'Alerta'
        sleep 1 #Sleep apenas para verificar visualmente o que está acontecendo
        msg = page.driver.browser.switch_to.alert.text
        expect(msg).to eql 'Isto é uma mensagem de alerta'
        sleep 1 #Sleep apenas para verificar visualmente o que está acontecendo
    end
end