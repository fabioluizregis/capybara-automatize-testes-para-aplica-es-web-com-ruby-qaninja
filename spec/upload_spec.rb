describe 'Upload', :upload do
    before(:each) do
        # Dir.pwd ==> Retorna o caminho onde está o projeto
        @arquivo = Dir.pwd + '/spec/fixtures/arquivo.txt'
        @imagem = Dir.pwd + '/spec/fixtures/imagem.png'
        visit '/upload'
    end

    it 'upload com arquivo texto' do
        # No momento desta aula, o FIREFOX tá com BUG para aceitar o attach_file com isso usamos o Chrome.
        attach_file('file-upload', @arquivo)  #attach_file(ELEMENTO,CAMINHO_DO_OBJETO)      
        click_button('Upload')
        
        div_arquivo = find('#uploaded-file')
        expect(div_arquivo.text).to eql 'arquivo.txt'
    end

    it 'upload de imagem' do
        attach_file('file-upload', @imagem)
        click_button('Upload')

        img = find('#new-image')
        expect(img[:src]).to include '/uploads/imagem.png'
    end

    after(:each) do
        sleep 1 # Sleep temporario, apenas para verificar o resultado visualmente
    end
end