describe 'Mouse hover', :hovers do
    before(:each) do
        visit '/hovers'
    end

    it 'quando passo o mouse sobre o blade' do
        card = find('img[alt=Blade]')
        card.hover

        expect(page).to have_content 'Nome: Blade'
    end

    it 'quando passo o mouse sobre o pantera negra' do
        card = find('img[alt="Pantera Negra"]')
        card.hover

        expect(page).to have_content 'Nome: Pantera Negra'
    end

    it 'quando passo o mouse sobre o homem aranha' do
        card = find('img[alt="Homem Aranha"]')
        card.hover

        expect(page).to have_content 'Nome: Homem Aranha'
    end

    it 'quando passo o mouse sobre o pantera negra com expressão regular - inicio' do
        card = find('img[alt^=Pantera]') # ^= diz que é um elemento que começa com "Pantera"
        card.hover 

        expect(page).to have_content 'Nome: Pantera Negra'
    end

    it 'quando passo o mouse sobre o homem aranha com expressão regular - final' do
        card = find('img[alt$=Aranha]') # $= diz que é um elemento que termina com "Aranha"
        card.hover

        expect(page).to have_content 'Nome: Homem Aranha'
    end

    it 'quando passo o mouse sobre o homem aranha com expressão regular - contem' do
        card = find('img[alt*=Aranha]') # *= diz que é um elemento que contém a palavra "Aranha"
        card.hover

        expect(page).to have_content 'Nome: Homem Aranha'
    end

    after(:each) do
        sleep 1 # Sleep temporario, apenas para verificar o resultado visualmente
    end
end