describe 'Forms' do
    it 'login com sucesso' do
        visit '/login'

        #fill_in preenche um campo, mapeado pelo name ou ID
        fill_in 'userId', with: 'stark'
        fill_in 'password', with: 'jarvis!'
        #click_button clica em um botão pelo name
        click_button 'Login'

        expect(find('#flash').visible?).to be true
        expect(find('#flash').text).to include 'Olá, Tony Stark. Você acessou a área logada!'
        expect(find('#flash')).to have_content 'Olá, Tony Stark. Você acessou a área logada!'
    end

    it 'senha incorreta' do
        visit '/login'

        #fill_in preenche um campo, mapeado pelo name ou ID
        fill_in 'userId', with: 'stark'
        fill_in 'password', with: 'senha_incorreta'
        #click_button clica em um botão pelo name
        click_button 'Login'

        expect(find('#flash').text).to include 'Senha é invalida!'
    end

    it 'usuário não cadastrado' do
        visit '/login'

        #fill_in preenche um campo, mapeado pelo name ou ID
        fill_in 'userId', with: 'fulano_jack'
        fill_in 'password', with: 'jarvis!'
        #click_button clica em um botão pelo name
        click_button 'Login'

        expect(find('#flash')).to have_content 'O usuário informado não está cadastrado!'
    end
end