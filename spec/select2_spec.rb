describe 'Select2', :select2 do
    describe('single', :single) do
        before(:each) do
            visit '/apps/select2/single.html'
        end

        it 'seleciona ator por nome' do
            find('.select2-selection--single').click
            sleep 1 #Sleep apenas para verificar visualmente o que está acontecendo
            find('.select2-results__option', text: 'Adam Sandler').click
            sleep 1 #Sleep apenas para verificar visualmente o que está acontecendo
        end

        it 'busca e clica por ator' do
            find('.select2-selection--single').click
            sleep 1 #Sleep apenas para verificar visualmente o que está acontecendo
            find('.select2-search__field').set 'Chris Rock'
            sleep 1 #Sleep apenas para verificar visualmente o que está acontecendo
            find('.select2-results__option').click
            sleep 1 #Sleep apenas para verificar visualmente o que está acontecendo
        end
    end

    describe('multiple', :multi) do
        before(:each) do
            visit '/apps/select2/multi.html'
        end 

        def seleciona(ator)
            find('.select2-search').click
            find('.select2-search__field').set ator
            find('.select2-results__option').click
        end

        it 'seleciona atores' do

            atores = ['Jim Carrey', 'Adam Sandler', 'Kevin James']

            atores.each do |a|
                seleciona(a)
            end
            sleep 1 #Sleep apenas para verificar visualmente o que está acontecendo
        end
    end
end