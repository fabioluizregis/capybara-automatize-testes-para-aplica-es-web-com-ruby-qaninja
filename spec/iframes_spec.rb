describe 'iframes' do
    describe 'bom', :nice_iframe do
        before(:each) do
            visit '/nice_iframe'
        end

        it 'adicionar ao carrinho' do
            # within_frame funciona com o ID apenas
            within_frame('burgerId') do
                produto = find('.menu-item-info-box', text: 'REFRIGERANTE')
                produto.find('a').click

                expect(find('#cart')).to have_content 'R$ 4,50'
                sleep 1 #Sleep apenas para verificar visualmente o que está acontecendo
            end
        end

    end

    describe 'ruim', :bad_iframe do
        before(:each) do
            visit '/bad_iframe'
        end

        it 'carrinho deve estar vazio' do

            # Adicionando um ID temporário pelo fato de o iFrame RUIM não ter ID
            # e o within_frame só trabalhar com ids
            script = '$(".box-iframe").attr("id", "tempID");'
            page.execute_script(script)

            within_frame('tempID') do
                expect(find('#cart')).to have_content 'Seu carrinho está vazio!'
                sleep 1 #Sleep apenas para verificar visualmente o que está acontecendo
            end
        end
    end
end