describe 'Login 2', :login2 do
    before(:each) do
        visit '/login2'
    end

    it 'com data de nascimento' do
        #fill_in preenche um campo, mapeado pelo name ou ID
        find('#userId').set 'stark'
        find('#passId').set 'jarvis!'

        login_fom = find('#login')
        case login_fom.text
        when /Dia/  # /TEXTO/ é usado como um CONTAINS TEXTO
            find('#day').set '29'
        when /Mês/
            find('#month').set '05'
        when /Ano/
            find('#year').set '1970'
        end
        sleep 1 #Sleep apenas para verificar visualmente o que está acontecendo


        #click_button clica em um botão pelo name
        click_button 'Login'

        expect(find('#flash')).to have_content 'Olá, Tony Stark. Você acessou a área logada!'
    end
end