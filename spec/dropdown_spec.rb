# :dropdown é como coloca a TAG no RSPEC

describe 'Caixa de opções', :dropdown do
    it 'item especifico simples' do
        visit '/dropdown'
        select('Loki', from: 'dropdown')
    end

    it 'item especifico com o find' do
        visit '/dropdown'
        drop = find('.avenger-list')
        drop.find('option', text: 'Scott Lang').select_option    
    end

    it 'qualquer item', :sample do
        visit '/dropdown'
        drop = find('.avenger-list')
        # all porque tem mais de um elemento option no retorno do find
        # sample para escolher qualquer elemento dentro do array aleatoriamente.
        drop.all('option').sample.select_option
    end
end