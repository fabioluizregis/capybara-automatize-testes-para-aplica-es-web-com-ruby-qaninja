describe 'Botoes de Radio', :radio do
    before(:each) do
        visit '/radios'
    end

    it 'seleção por id' do
        choose('cap')  #choose aceita diretamente colocando o id do elemento
    end

    it 'seleção por find e ccs selector' do
        find('input[value="the-avengers"]').click
    end

    after(:each) do
        sleep 1
    end
end