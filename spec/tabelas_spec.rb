describe 'Tabelas', :tabs do
    before(:each) do
        visit '/tables'
    end

    it 'deve exibir o salário do stark' do
        # uso o all() porque retorna mais de um elemento e nesse caso o find() não funciona
        atores = all('table tbody tr')
        # detect funciona igual ao foreach() só que para tabelas, e retorna as informações da linha da tabela.
        stark = atores.detect{ |ator| ator.text.include?('Robert Downey Jr')}
        puts stark.text 

        expect(stark.text).to include '10.000.000'
    end

    it 'deve exibir o salario do vin diesel' do
        diesel = find('table tbody tr', text: '@vindiesel')
        expect(diesel).to have_content '10.000.000'
    end

    it 'deve exibir o filme velozes' do
        diesel = find('table tbody tr', text: '@vindiesel')
        movie = diesel.all('td')[2].text

        expect(movie).to eql 'Velozes e Furiosos'
    end

    it 'deve exibir o insta do Chris Evans' do
        evans = find('table tbody tr', text: 'Chris Evans')
        insta = evans.all('td')[4].text

        expect(insta).to eql '@teamcevans'
    end

    it 'remove chriss pratt' do
        prat = find('table tbody tr', text: 'Chris Pratt')
        prat.find('a', text: 'delete').click

        msg = page.driver.browser.switch_to.alert.text
        expect(msg).to eql 'Chris Pratt foi selecionado para remoção!'
    end

    it 'edit chriss pratt' do
        prat = find('table tbody tr', text: 'Chris Pratt')
        prat.find('a', text: 'edit').click

        msg = page.driver.browser.switch_to.alert.text
        expect(msg).to eql 'Chris Pratt foi selecionado para edição!'
    end
     
end