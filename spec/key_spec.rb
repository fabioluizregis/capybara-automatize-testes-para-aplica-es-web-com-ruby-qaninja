describe 'teclado', :key do
    before(:each) do
        visit '/key_presses'
    end

    it 'enviando teclas' do
        # %i cria um array de símbolos
        teclas = %i[escape tab space enter shift control alt]

        teclas.each do |t|
            find('input[id=campo-id]').send_keys t
            expect(page).to have_content 'You entered: ' + t.to_s.upcase
            sleep 1 #Sleep apenas para verificar visualmente o que está acontecendo
        end 
    end

    it 'enviando letras' do
        # %w cria um array de strings
        teclas = %w[a s d f g h j k l]

        teclas.each do |t|
            find('#campo-id').send_keys t
            expect(page).to have_content 'You entered: ' + t.upcase
            sleep 1 #Sleep apenas para verificar visualmente o que está acontecendo
        end 
    end
end